import 'package:flutter/material.dart';

enum APP_THEME{LIGHT,DARK}

void main() {
  runApp(ContactProfilePage());
}

class ContactProfilePage extends StatefulWidget{
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}


class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme=APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme ==APP_THEME.LIGHT
      ? MyAppTheme.appThemeLight()
      :MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget,
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.nightlight),
          onPressed: (){
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme=APP_THEME.LIGHT
                  : currentTheme=APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}

class MyAppTheme{
  static ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Colors.white24,
            iconTheme: IconThemeData(
                color: Colors.black
            ),
        ),
      iconTheme: IconThemeData(
          color: Colors.red
      ),
    );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
            color: Colors.black54,
            iconTheme: IconThemeData(
                color: Colors.white
            )
        ),
      iconTheme: IconThemeData(
          color: Colors.red
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
      children: <Widget>[
      IconButton(
      icon: Icon(
      Icons.call,
      // color: Colors.indigo.shade800,
  ),
  onPressed: () {},
  ),
  Text("Call"),
  ],);
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.textsms_rounded,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],);
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],);
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.mail,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],);
}

Widget buildDirectionButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],);
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money_outlined,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],);
}

Widget mobilePhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("092-503-5906"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon:  Icon(Icons.message),
      // color: Colors.pink,
      onPressed: (){},
    ),
  );
}

Widget otherPhoneListTile(){
  return ListTile(
    leading: Icon(null),
    title: Text("092-382-5650"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon:  Icon(Icons.message),
      // color: Colors.pink,
      onPressed: (){},
    ),
  );
}

Widget emailListTile(){
  return ListTile(
    leading: Icon(Icons.mail),
    title: Text("63160277@go.buu.ac.th"),
    subtitle: Text("work"),
    trailing:  Icon(null),
  );
}

Widget addressListTile(){
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("Thawung Lopburi 15150"),
    subtitle: Text("home"),
    trailing:  IconButton(
      icon:  Icon(Icons.directions),
      // color: Colors.pink,
      onPressed: (){},
    ),
  );
}

var buildAppBarWidget =
AppBar(
    // backgroundColor: Colors.white24,
      leading: Icon(Icons.arrow_back,
        // color: Colors.black,
      ),
      actions: <Widget>[
        IconButton(onPressed: (){},
            icon: Icon(Icons.star_border,
              // color: Colors.black,
            )
        )
      ]
  );


Widget buildBodyWidget(){
  return ListView(

    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 250,

            child: Image.network(
              "https://i.imgur.com/wgGeqIE.png",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child:Text("Adsadawut Tadyoo",
                      style: TextStyle(fontSize: 30)
                  ),
                )
              ],
            ),
          ),
          // Divider(
          //   color: Colors.grey,
          // ),
          // Container(
          //   margin: const EdgeInsets.only(top:8,bottom: 8),
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //     children: <Widget>[
          //       buildCallButton(),
          //       buildTextButton(),
          //       buildVideoCallButton(),
          //       buildEmailButton(),
          //       buildDirectionButton(),
          //       buildPayButton()
          //     ],
          //   ),
          // ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: const EdgeInsets.only(top:8,bottom:8),
            child: Theme(
              data:ThemeData(
                iconTheme: IconThemeData(
                  color: Colors.green
                ),
              ),
            child: profileActionItem(),
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          mobilePhoneListTile(),
          otherPhoneListTile(),
          emailListTile(),
          addressListTile(),
        ],
      )
    ],
  );
}

Widget profileActionItem(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionButton(),
      buildPayButton(),
    ],
  );
}